FROM openliberty/open-liberty:22.0.0.6-kernel-slim-java17-openj9-ubi

ADD target/config-service-0.0.1-SNAPSHOT.jar /home

CMD ["java","-jar","home/config-service-0.0.1-SNAPSHOT.jar"]

EXPOSE 8888